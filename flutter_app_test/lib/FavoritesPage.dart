import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'dart:math' show pi;
import 'package:flutter_app_test/CustomWidgets/BGImageSettings.dart';
import 'package:flutter_app_test/Login.dart';

class Favorites extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: Colors.blueGrey,
              ),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.center,
                    child: Text("Menu", style: TextStyle(
                        fontSize: 20.0, color: Colors.white),),)
                ],
              ),
            ),
            new ListTile(
              title: new Text('Logout', style: TextStyle(
                  fontSize: 20.0, color: Colors.black),),
              onTap: () {
                Navigator.push(context,
                  MaterialPageRoute(builder: (context) => LoginScreen()),);
              },
            ),

          ]
        )
      ),
      appBar: AppBar(
        title: Text('My Favorites'),
        backgroundColor: Colors.blueGrey,
      ),
      body: Stack(
          children:
          [
            BackgroundImage(imageLocation: 'images/Food2.jpg'),
            GridView.count(
              crossAxisCount: 2,
              children: [
                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/1.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                          color: Colors.white70,
                          child:  RichText(
                              text: TextSpan(
                                  style: TextStyle(fontSize: 15.0, color: Colors.black),
                                  children: <TextSpan> [
                                    TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                    TextSpan(text: " Azteca"),
                                    TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                    TextSpan(text: " 13USD"),
                                    TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                    TextSpan(text: " 10/10"),
                                  ]
                              )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/2.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Rosa Cafe"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 5USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 8/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/3.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Passporte"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 22USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 7/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/4.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Bo's House"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 7USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 6/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/5.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Eskander's"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 15USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 10/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/6.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Cafe Tropicale"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 8USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 6/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/7.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Rapallino"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 15USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 10/10"),
                                ]
                            )),
                      )
                  ),
                ),

                Container(
                  height: 100,
                  width: 50,
                  child: WidgetFlipper(
                      frontWidget: Container(
                        padding: EdgeInsets.all(2.0),
                        decoration: BoxDecoration(
                          color: Colors.black,
                          image: const DecorationImage(
                            image: AssetImage("images/8.jpg"),
                            fit: BoxFit.cover,
                          ),
                          border: Border.all(
                            color: Colors.black,
                            width: 1,
                          ),
                          borderRadius: BorderRadius.circular(12),
                        ),
                      ),
                      backWidget: Container(
                        alignment: Alignment.center,
                        color: Colors.white70,
                        child:  RichText(
                            text: TextSpan(
                                style: TextStyle(fontSize: 15.0, color: Colors.black),
                                children: <TextSpan> [
                                  TextSpan(text: "Restaurant:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " Burger Crave"),
                                  TextSpan(text: "\n Price:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 10USD"),
                                  TextSpan(text: "\n Rating:", style: TextStyle(fontWeight: FontWeight.bold)),
                                  TextSpan(text: " 8/10"),
                                ]
                            )),
                      )
                  ),
                ),
              ]
            )
          ]
      ),
    );
  }
}

class WidgetFlipper extends StatefulWidget {
  WidgetFlipper({
    Key key,
    this.frontWidget,
    this.backWidget,
  }) : super(key: key);

  final Widget frontWidget;
  final Widget backWidget;

  @override
  _WidgetFlipperState createState() => _WidgetFlipperState();
}

class _WidgetFlipperState extends State<WidgetFlipper> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> _frontRotation;
  Animation<double> _backRotation;
  bool isFrontVisible = true;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(duration: Duration(milliseconds: 500), vsync: this);
    _frontRotation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: Tween(begin: 0.0, end: pi / 2).chain(CurveTween(curve: Curves.linear)),
          weight: 50.0,
        ),
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(pi / 2),
          weight: 50.0,
        ),
      ],
    ).animate(controller);
    _backRotation = TweenSequence(
      <TweenSequenceItem<double>>[
        TweenSequenceItem<double>(
          tween: ConstantTween<double>(pi / 2),
          weight: 50.0,
        ),
        TweenSequenceItem<double>(
          tween: Tween(begin: -pi / 2, end: 0.0).chain(CurveTween(curve: Curves.linear)),
          weight: 50.0,
        ),
      ],
    ).animate(controller);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      children: [
        AnimatedCard(
          animation: _backRotation,
          child: widget.backWidget,
        ),
        AnimatedCard(
          animation: _frontRotation,
          child: widget.frontWidget,
        ),
        _tapDetectionControls(),
      ],
    );
  }

  Widget _tapDetectionControls() {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        GestureDetector(
          onTap: _leftRotation,
          child: FractionallySizedBox(
            widthFactor: 0.5,
            heightFactor: 1.0,
            alignment: Alignment.topLeft,
            child: Container(
              color: Colors.transparent,
            ),
          ),
        ),
        GestureDetector(
          onTap: _rightRotation,
          child: FractionallySizedBox(
            widthFactor: 0.5,
            heightFactor: 1.0,
            alignment: Alignment.topRight,
            child: Container(
              color: Colors.transparent,
            ),
          ),
        ),
      ],
    );
  }

  void _leftRotation() {
    _toggleSide();
  }

  void _rightRotation() {
    _toggleSide();
  }

  void _toggleSide() {
    if (isFrontVisible) {
      controller.forward();
      isFrontVisible = false;
    } else {
      controller.reverse();
      isFrontVisible = true;
    }
  }
}

class AnimatedCard extends StatelessWidget {
  AnimatedCard({
    this.child,
    this.animation,
  });

  final Widget child;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: animation,
      builder: (BuildContext context, Widget child) {
        var transform = Matrix4.identity();
        transform.setEntry(3, 2, 0.001);
        transform.rotateY(animation.value);
        return Transform(
          transform: transform,
          alignment: Alignment.center,
          child: child,
        );
      },
      child: child,
    );
  }
}

















