import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter_app_test/CustomWidgets/TextSettings.dart';
import 'package:flutter_app_test/CustomWidgets/BGImageSettings.dart';
import 'package:flutter_app_test/CustomWidgets/TextInputSettings.dart';
import 'package:flutter_app_test/CustomWidgets/PasswordInputSettings.dart';
import 'dart:async';

Widget loginButton = Text("Login", style: TextStyle(fontSize: 25));
int loginDelay = 3;

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool _visible = true;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        //from BGImageSettings.dart
        BackgroundImage(
          imageLocation: 'images/food.jpg',
        ),

        Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            children: [
              Flexible(
                child: Center(
                  child: Text(
                    'FoodApp',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 60,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AnimatedOpacity(
                    opacity: _visible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 500),
                    child: TextInputField(
                      icon: FontAwesomeIcons.envelope,
                      hint: 'Email',
                      inputType: TextInputType.emailAddress,
                      inputAction: TextInputAction.next,
                    ),
                  ),
                  AnimatedOpacity(
                    opacity: _visible ? 1.0 : 0.0,
                    duration: Duration(milliseconds: 500),
                    child: PasswordInput(
                      icon: FontAwesomeIcons.lock,
                      hint: 'Password',
                      inputAction: TextInputAction.done,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Container(
                    width: 300,
                    height: 50,
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        side: BorderSide(color: Colors.blueAccent),
                      ),
                      onPressed: () {
                        //Change the login text into a loading icon
                        setState(() {
                          loginButton = CircularProgressIndicator(
                            backgroundColor: Colors.blueGrey,
                          );
                          _visible = !_visible;
                        });
                        //Since there is no actual login, delay the next page
                        Timer(Duration(seconds: loginDelay), () {
                          //Change icon back to login text
                          setState(() {
                            loginButton =
                                Text("Login", style: TextStyle(fontSize: 25));
                            _visible = !_visible;
                          });
                          //goto Favorites Page
                          Navigator.pushReplacementNamed(context, 'FavoritesPage');
                        });
                      },
                      color: Colors.blueAccent,
                      textColor: Colors.white,
                      child: loginButton,
                    ),
                  ),
                  SizedBox(
                    height: 25,
                  ),
                ],
              ),
              AnimatedOpacity(
                opacity: _visible ? 1.0 : 0.0,
                duration: Duration(milliseconds: 500),
                child: GestureDetector(
                  onTap: () => Navigator.pushNamed(context, 'CreateNewAccount'),
                  child: Container(
                    child: Text(
                      'Create New Account',
                      style: TextBodyStyle,
                    ),
                    decoration: BoxDecoration(
                        border: Border(
                            bottom: BorderSide(width: 1, color: constWhite))),
                  ),
                ),
              ),
              SizedBox(
                height: 20,
              ),
            ],
          ),
        )
      ],
    );
  }
}
